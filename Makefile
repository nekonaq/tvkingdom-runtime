SHELL		= /bin/sh

run		= $(null)
top_dir		= .
app_dir		= $(abspath $(top_dir))

MAKE_ENVFILE	= .env
ifneq ($(wildcard $(MAKE_ENVFILE)),)
# ifeq ($(MAKELEVEL),0)
# $(info '## Makefile using envfile: $(MAKE_ENVFILE)')
# endif
include $(MAKE_ENVFILE)
SERVICE		= $(COMPOSE_PROJECT_NAME)
else
SERVICE		= $(notdir $(PWD))
endif

DESTDIR		=
INSTALL_OWNER	= root
INSTALL_GROUP	= root

SERVICE_PREFIX	= $(null)
SERVICE_NAME	= $(SERVICE_PREFIX)$(SERVICE)
UNITDIR_LIB	= $(DESTDIR)/lib/systemd/system
UNITDIR_ETC	= $(DESTDIR)/etc/systemd/system
TMPFILES_ETC	= $(DESTDIR)/etc/tmpfiles.d
SYSCONFDIR	= $(DESTDIR)/etc/default

SYSTEMD_SERVICE		= systemd.service
SYSTEMD_SERVICE_CONF	= $(UNITDIR_LIB)/$(SERVICE_NAME).service

# カレントディレクトリに service.timer があれば仕込む
SYSTEMD_TIMER		= systemd.timer
SYSTEMD_TIMER_CONF	= $(if $(wildcard $(SYSTEMD_TIMER)),$(UNITDIR_LIB)/$(SERVICE_NAME).timer)

SYSTEMD_TMPFILES	= systemd.tmpfiles.conf
SYSTEMD_TMPFILES_CONF	= $(if $(wildcard $(SYSTEMD_TMPFILES)),$(TMPFILES_ETC)/$(SERVICE_NAME).conf)

# #//-----------------------------------------------------------------------------
# SERVICE_UNIT	= $(SERVICE).service
# TIMER_UNIT	= $(wildcard $(SERVICE).timer)
# TMPFILES_CONF	= $(wildcard $(SERVICE)-tmpfiles.conf)

# SYSTEMD_SERVICE		= $(UNITDIR_LIB)/$(SERVICE_UNIT)
# SYSTEMD_TIMER		= $(if $(TIMER_UNIT),$(UNITDIR_LIB)/$(TIMER_UNIT))
# SYSTEMD_TMPFILES	= $(if $(TMPFILES_CONF),$(TMPFILES_ETC)/$(SERVICE).conf)

#------------------------------------------------------------------------
all:; $(error No target specified)

#//
install remove name status start stop enable disable:
	@$(MAKE) --no-print-directory $(SERVICE).service--$@

%.service--name:
	@( echo $*.service; \
	   $(if $(SYSTEMD_TIMER_CONF),echo $*.timer;) \
	)

%.service--install: $(SYSTEMD_SERVICE_CONF) $(SYSTEMD_OVERRIDE_FILE) $(SYSTEMD_TIMER_CONF) $(SYSTEMD_TMPFILES_CONF)
	@( set -x; $(run) systemctl daemon-reload )

%.service--remove:
	@( set -x; \
	   $(if $(SYSTEMD_TIMER_CONF),$(run) systemctl stop $*.timer ||:;) \
	   $(if $(SYSTEMD_TIMER_CONF),$(run) systemctl disable $*.timer ||:;) \
	   $(run) systemctl stop $*.service ||:; \
	   $(run) systemctl disable $*.service ||: ; \
	   $(run) rm -rf $(SYSTEMD_SERVICE_CONF) $(SYSTEMD_OVERRIDE_FILE) $(SYSTEMD_TIMER_CONF) $(SYSTEMD_TMPFILES_CONF); \
	   $(run) systemctl daemon-reload; \
	)

$(SYSTEMD_SERVICE_CONF): $(SYSTEMD_SERVICE)
	@( $(if $(wildcard $(MAKE_ENVFILE)),eval "$$( sed -e '\!^#!d; \!^$$!d; s!^!export !; s!=\(.*\)!="\1";!' $(MAKE_ENVFILE) )"); \
	   export app_dir='$(app_dir)'; \
	   cat $< | envsubst | ( set -x; \
	     $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 644 /dev/stdin $@; \
	   )\
	)

.PHONY: install remove name status start stop enable disable
.PHONY: $(SYSTEMD_SERVICE_CONF) $(SYSTEMD_TIMER_CONF)

#//
ifneq (,$(SYSTEMD_TIMER_CONF))
status.timer start.timer stop.timer enable.timer disable.timer:
	@$(MAKE) --no-print-directory $(SERVICE_NAME).timer--$(@:.timer=)

.PHONY: status.timer start.timer stop.timer enable.timer disable.timer

$(SYSTEMD_TIMER_CONF): $(SYSTEMD_TIMER)
	@( set -x; \
	   $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 644 $< $@; \
	)

.PHONY: $(SYSTEMD_TIMER_CONF)
endif

#//
ifneq (,$(SYSTEMD_TMPFILES_CONF))
$(SYSTEMD_TMPFILES_CONF): $(SYSTEMD_TMPFILES)
	@( $(if $(wildcard $(MAKE_ENVFILE)),eval "$$( sed -e '\!^#!d; \!^$$!d; s!^!export !; s!=\(.*\)!="\1";!' $(MAKE_ENVFILE) )"); \
	  set -x; \
	  cat $< | envsubst | \
	    $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 644 /dev/stdin $@; \
	)
endif

#//
%--status %--start %--stop %--disable %--enable:
	@( set -x; $(run) systemctl $(subst $*--,,$@) $* ||: )

.PHONY: %--name %--status %--start %--stop %--disable %--enable

# Local Variables:
# fill-column: 70
# End:
# #//-----------------------------------------------------------------------------


# $(SERVICE)--remove:
# 	@( set -x; \
# 	   $(run) systemctl stop  $(if $(TIMER_UNIT),$(TIMER_UNIT),$(SERVICE_UNIT)) ||:; \
# 	   $(run) systemctl disable  $(if $(TIMER_UNIT),$(TIMER_UNIT),$(SERVICE_UNIT)) ||:; \
# 	   $(run) rm -rf $(SYSTEMD_SERVICE) \
# 	     $(if $(TIMER_UNIT),$(SYSTEMD_TIMER)) \
# 	     $(if $(TMPFILES_CONF),$(SYSTEMD_TMPFILES)) \
# 	     ; \
# 	   $(run) systemctl daemon-reload; \
# 	)

# .PHONY: $(SYSTEMD_SERVICE) $(SYSTEMD_TIMER) $(SYSTEMD_TMPFILES)

# #//
# %--status %--start %--stop %--disable %--enable:
# 	@( set -x; $(run) systemctl $(subst $*--,,$@) $*.$(if $(TIMER_UNIT),timer,service) ||: )

# .PHONY: %--name %--status %--start %--stop %--disable %--enable

# # Local Variables:
# # fill-column: 70
# # End:
